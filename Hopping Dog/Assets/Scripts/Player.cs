﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float jumpMagnitude = 150f;
    public Rigidbody2D _rigidbody2D;
    public Animator playerAnimator;

    Collider2D ground;

    bool isGrounded = true;

	void OnEnable ()
    {
        ground = GameObject.FindGameObjectWithTag("Ground").GetComponent<BoxCollider2D>();
        LevelManager.Instance.currentScore = 0;
        LevelManager.Instance.SetScore(0);
	}
	
	void FixedUpdate ()
    {

#if UNITY_EDITOR || UNITY_WEBGL	

        jumpMagnitude = 300;
        if (Input.GetKeyDown(KeyCode.Space) && gameObject.GetComponent<BoxCollider2D>().IsTouching(ground))
        {
            //Make player jump
            Jump();
            isGrounded = false;
        }
#elif !UNITY_EDITOR

        if (Input.GetTouch(0).tapCount == 1 && gameObject.GetComponent<BoxCollider2D>().IsTouching(ground))
        {
            //Make player jump
           Jump();
           isGrounded = false;
        }

#endif


    }
   

    public void OnCollisionEnter2D(Collision2D other)
    {

        if(isGrounded == false && other.gameObject.CompareTag("Ground"))
        {
            playerAnimator.SetBool("Jump", false);
            isGrounded = true;
        }
        else if (other.gameObject.CompareTag("Kill"))
        {
            Death();
        }

       
    }

    void Jump()
    {
        AudioManager.PlayEffects("Jump");
        _rigidbody2D.velocity = Vector3.zero;
        _rigidbody2D.AddForce(new Vector2(0f, jumpMagnitude));
        playerAnimator.SetBool("Jump", true);
    }

    void Death()
    {
        AudioManager.PlayEffects("Death");
        AudioManager.StopEffects("Music");

        playerAnimator.SetBool("Death", true);
        Destroy(GetComponent<Rigidbody2D>());
        LevelManager.Instance.Kill();

    }
   
}
