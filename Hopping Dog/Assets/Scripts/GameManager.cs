﻿using UnityEngine;
using GooglePlayGames;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject Game;
    public GameObject MainMenu;
    public GameObject SettingsPanel;
    public GameObject InfoPanel;

    public GameObject leadboardButton;
    public GameObject achievmentButton;

    public Button soundButton;

    public Sprite soundOn;
    public Sprite soundOff;



    float check = 0;

    void Awake()
    {
      PlayerPrefs.SetInt("isSoundOff", 1);

#if UNITY_EDITOR || UNITY_WEBGL
        leadboardButton.SetActive(false);
        achievmentButton.SetActive(false);
#endif

        if (PlayerPrefs.GetInt("isSoundOff") == 0)
        {
            soundButton.image.sprite = soundOn;
            AudioManager.PlayEffects("Music");
        }
        else
        {
            soundButton.image.sprite = soundOff;
        }
    }

    void Start()
    { 
        PlayGamesPlatform.Activate();
        // authenticate user:
        Social.localUser.Authenticate((bool success) => {
            // handle success or failure
            if (success)
                check = 1;
            else
                check = 2;
        });
    }
	
    public void StartGame()
    {
        SettingsPanel.transform.localScale = Vector3.zero;

        if (check == 1 || check == 2)
        {
            if (InfoPanel.transform.localScale.x > Vector3.zero.x)
            {
                AudioManager.PlayEffects("ButtonClick");

                iTween.ScaleTo(InfoPanel, Vector3.zero, 1f);
            }
            else
            {
             //   AudioManager.StopEffects("Music");
                AudioManager.PlayEffects("ButtonClick");
                MainMenu.SetActive(false);
                Game.SetActive(true);
            }
        }
    }

    public void SettingButton()
    {
        AudioManager.PlayEffects("ButtonClick");

        iTween.ScaleTo(SettingsPanel, Vector3.one, 1f);
        iTween.ScaleTo(InfoPanel, Vector3.zero, 1f);
    }
    public void InfoButton()
    {
        AudioManager.PlayEffects("ButtonClick");

        iTween.ScaleTo(SettingsPanel, Vector3.zero, 1f);
        iTween.ScaleTo(InfoPanel, Vector3.one, 1f);
    }
    public void LeaderBoardButton()
    {
        AudioManager.PlayEffects("ButtonClick");
        iTween.ScaleTo(SettingsPanel, Vector3.zero, 1f);
        iTween.ScaleTo(InfoPanel, Vector3.zero, 1f);

        PlayGamesPlatform.Instance.ShowLeaderboardUI(HoppingDogConstants.leaderboard_top_scorers);
    }

    public void AchievmentBoardButton()
    {
        AudioManager.PlayEffects("ButtonClick");
        iTween.ScaleTo(SettingsPanel, Vector3.zero, 1f);
        iTween.ScaleTo(InfoPanel, Vector3.zero, 1f);

        Social.ShowAchievementsUI();
    }

    public void BackButton()
    {
      //  AudioManager.PlayEffects("Music");
        AudioManager.PlayEffects("ButtonClick");
        LevelManager.Instance.Reset();
        
        Game.SetActive(false);
        MainMenu.SetActive(true);
        
    }

    public void Sound()
    {
        if (PlayerPrefs.GetInt("isSoundOff") == 0)
        {
            AudioManager.PauseEffects("Music");
            soundButton.image.sprite = soundOff;
            PlayerPrefs.SetInt("isSoundOff", 1);
        }
        else
        {
            soundButton.image.sprite = soundOn;
            PlayerPrefs.SetInt("isSoundOff", 0);
            AudioManager.PlayEffects("Music");
        }
    }
 
}
