﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class LevelManager : MonoBehaviour {

    public static LevelManager Instance { get; set; }

    public GameObject player;
    public ObstacleController obstacles;
    public CloudController clouds;
    public Text t_score;
    public Text t_highScore;
    public GameObject gameEnd;
    public GameObject buttonRestart;

    [HideInInspector]
    public bool isDead = false;

    [HideInInspector]
    public int currentScore;

    private float score;
    private int highScore;
    private GameObject currentPlayer;


    void Awake()
    {
        Instance = this;
       
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("FirstPlay") == 0)
        {
            PlayerPrefs.SetInt("FirstPlay", 1);
            Social.ReportProgress(HoppingDogConstants.achievement_first_chase, 100.0f, (bool success) => {
                // handle success or failure
            });
        }
    }

    void OnEnable ()
    {
       highScore = PlayerPrefs.GetInt("Highscore");
       t_highScore.text = t_highScore.text.Split(':')[0];
       t_highScore.text += ": ";
       t_highScore.text += highScore;
       
       SpawnPlayer();
	}
	void Update()
    {

        if (!isDead)
        {
            score += (0.15f * (1 + Time.maximumDeltaTime));

            if (Mathf.RoundToInt(score) % 5 == 0)
            {
                currentScore = Mathf.RoundToInt(score)/10;

                SetScore(currentScore);
            }

            if (currentScore > highScore)
            {
                highScore = currentScore;
                PlayerPrefs.SetInt("Highscore", highScore);
                t_highScore.text = t_highScore.text.Split(':')[0];
                t_highScore.text += ": ";
                t_highScore.text += highScore;
            }
        }
    }

    public void Kill()
    {
        buttonRestart.GetComponent<Button>().enabled = true;
        // post score 12345 to leaderboard ID "Cfji293fjsie_QA")
        Social.ReportScore(currentScore, HoppingDogConstants.leaderboard_top_scorers, (bool success) =>
            {
                // handle success or failure
            });

        this.score = 0;
        currentScore = 0;
        isDead = true;
        iTween.ScaleTo(gameEnd, Vector3.one, 0.3f);
    }

    public void Restart()
    {
        buttonRestart.GetComponent<Button>().enabled = false;
        AudioManager.PlayEffects("ButtonClick");
        Reset();
        SpawnPlayer();
    }

    public void Reset()
    {
        iTween.ScaleTo(gameEnd, Vector3.zero, 0.2f);
        Destroy(currentPlayer);
        ResetElements();
    }

    public void SetScore(float score)
    {
        t_score.text = t_score.text.Split(':')[0];
        t_score.text += ": ";
        t_score.text += score;

        if (score == 100)
        {
            Social.ReportProgress(HoppingDogConstants.achievement_100_points_milestone, 100.0f, (bool success) =>
            {
                // handle success or failure
            });
        }
        else if (score == 500)
        {
            Social.ReportProgress(HoppingDogConstants.achievement_500_points_milestone, 100.0f, (bool success) =>
            {
                // handle success or failure
            });
        }
        else if (score == 1000)
        {
            Social.ReportProgress(HoppingDogConstants.achievement_1000_points_milestone, 100.0f, (bool success) =>
            {
                // handle success or failure
            });
        }
        else if (score == 2000)
        {
            Social.ReportProgress(HoppingDogConstants.achievement_2000_points_milestone, 100.0f, (bool success) =>
            {
                // handle success or failure
            });
        }
    }

    public void SpawnPlayer()
    {
        isDead = false;
        currentPlayer = Instantiate(player) as GameObject;
    }

    public void ResetElements()
    {
        obstacles.Reset();
        AudioManager.PlayEffects("Music");
        clouds.Reset();
    }
    void OnDisable()
    {
        gameEnd.transform.localScale = Vector3.zero;
    }
}
