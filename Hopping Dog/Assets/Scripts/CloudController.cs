﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CloudController : MonoBehaviour {

    public float speed = 1f;
    public List<GameObject> clouds;

    private int first = 0;
    private int current = 0;

    void OnEnable ()
    {
        first = Random.Range(0, clouds.Count);

        for (int i = 0; i < clouds.Count; i++)
        {
            clouds[i].SetActive(false);

            if (i == first)
                clouds[first].SetActive(true);
        }

    }

    void Update ()
    {
        if (transform.position.x < -18.97f)
        {
            transform.position = new Vector2(2.9f, 2.43f);

            current = Random.Range(0, clouds.Count);

            for (int i = 0; i < clouds.Count; i++)
            {
                clouds[i].SetActive(false);

                if (i == current)
                    clouds[current].SetActive(true);
            }

        }

        if (!LevelManager.Instance.isDead)
            transform.Translate(Vector3.left * Time.deltaTime * speed);
    }

    public void Reset()
    {
        transform.position = new Vector2(2.9f, 2.43f);
        current = Random.Range(0, clouds.Count);

        for (int i = 0; i < clouds.Count; i++)
        {
            clouds[i].SetActive(false);

            if (i == current)
                clouds[current].SetActive(true);
        }
    }
}
